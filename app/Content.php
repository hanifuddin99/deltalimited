<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    public function subnavmenu()
    {
        return $this->hasOne('App\Subnavmenu');
    }
}
