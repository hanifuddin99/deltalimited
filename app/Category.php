<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'main_category_id'
    ];
    
    public function maincategory()
    {
        return $this->belongsTo(MainCategory::class,'main_category_id', 'id');
    }
    public function products()
    {
        return $this->hasMany(Product::class);
    }
    
}

