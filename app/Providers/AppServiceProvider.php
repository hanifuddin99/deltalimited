<?php

namespace App\Providers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Navmenu;
use App\Subnavmenu;
use App\Subsubnavmenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $navmenus = Navmenu::orderBy('created_at','asc')->get();
        View::share('navmenus',$navmenus);
        $subnavmenus = Subnavmenu::orderBy('created_at','asc')->get();
        View::share('subnavmenus', $subnavmenus);
        $subsubnavmenus = Subsubnavmenu::orderBy('created_at','asc')->get();
        View::share('subsubnavmenus', $subsubnavmenus);
        $subsubnavmenus1 = Subsubnavmenu::where('subnavmenu_id',2)->orderBy('subsubnavmenu_name','asc')->get();
        View::share('subsubnavmenus1',$subsubnavmenus1);
        $subsubnavmenus2 = Subsubnavmenu::where('subnavmenu_id',11)->get();
        View::share('subsubnavmenus2',$subsubnavmenus2);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
