<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Navmenu extends Model
{
    public function submenus()
    {
        return $this->hasMany(Subnavmenu::class);
    }
}
