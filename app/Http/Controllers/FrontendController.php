<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Content;
use App\Product;
use App\MainCategory;
use App\Category;
use App\News;
use App\Album;
use App\Image;
use App\Video;
use App\Director;


class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::orderBy('created_at','asc')->get();
        $total_clients_row = Client::count();
        return view('frontend.pages.index',compact('clients','total_clients_row'));
    }
    public function messages($name)
    {
        if($name == 'board_of_directors'){
            $directors = Director::all();
            $content = Content::select('title','image')->where('title',$name)->first();
            return view('frontend.pages.directors',compact('directors','content'));
        }
        $contents=Content::where('title',$name)->first();
        return view('frontend.pages.contents',compact('contents'));
    }
    public function products($id)
    {
        if($id=='industrial' || $id == 'medical'){
            if($id == 'industrial'){
               $categories = Category::where('main_category_id',2)->get();  
            }else{
              $categories = Category::all();  
            }
          $products = Product::all();
          return view('frontend.pages.show_products',compact('products','categories'));
        }else{
         $categories = Category::all();  
         $products = Product::where('category_id',$id)->orderBy('created_at','asc')->get();
         return view('frontend.pages.show_products_individual',compact('products','categories'));
        }
    }
    public function news()
    {
        $news = News::all();
        $news1 = News::all();
        $total_news_row = News::count();
        return view('frontend.pages.news',compact('news','news1','total_news_row'));
    }
    public function contact()
    {
        return view('frontend.pages.contact');
    }
    public function photos()
    {
        $images=Album::orderBy('created_at','asc')->get();
        return view('frontend.pages.images_box',compact('images'));
    }
   
    public function photo_details($id)
    {
        $images = Image::where('album_id',$id)->orderBy('created_at','asc')->get();
        return view('frontend.pages.images',compact('images'));
    }
    public function videos(){
        $videos = Video::all();
        return view('frontend.pages.videos',compact('videos'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
