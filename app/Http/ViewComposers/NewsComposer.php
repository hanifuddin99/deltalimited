<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\News;

class NewsComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $news;
    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
       $this->news=News::get();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('news',$this->news);
    }
}