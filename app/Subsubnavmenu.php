<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsubnavmenu extends Model
{
    public function subnavmenu()
    {
        return $this->belongsTo(Subnavmenu::class);
    }
}
