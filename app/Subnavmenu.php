<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subnavmenu extends Model
{
    public function navmenu()
    {
        return $this->belongsTo(Navmenu::class);
    }
    public function submenus()
    {
        return $this->hasMany(Subsubnavmenu::class);
    }
    
    public function content()
    {
        return $this->belongsTo('App\Content');
    }
}
