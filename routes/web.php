<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','FrontendController@index')->name('/');
Route::get('/news','FrontendController@news')->name('news_and_events');
Route::get('/photos','FrontendController@photos')->name('photos');
Route::get('/photo_details/{id}','FrontendController@photo_details')->name('photo_details');
Route::get('/videos','FrontendController@videos')->name('videos');
Route::get('/products/{id}','FrontendController@products')->name('products');
Route::get('/abouts','FrontendController@abouts')->name('abouts');
Route::get('/contact','FrontendController@contact')->name('contact');
Route::get('/messages/{name}','FrontendController@messages')->name('messages');



Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
