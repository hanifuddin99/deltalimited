@extends('frontend.layouts.master')
@section('title','Products')
@section('content')

<section id='title_name'>
    <div class='container-fluid'>
        <div class='row'>
            <div class='col-12 col-sm-12 col-md-12 col-lg-12 jumbotron tab-center'>
                <div class='row'>
                    <div class='col-12'>
                         <h5 class='text-center text-uppercase text-muted'>
                             {{$menu_active=basename(Request::path())}} equipments 
                         </h5>
                        <ul class='nav nav-pills mb-3 uppercase-for-products-tab' id='pills-tab' role='tablist'>  
                            @foreach($categories as $category)
                            @if($category->category_name == 'washing')    
                            <li class="nav-item">
                            <a class="nav-link active btn btn-outline-info" type='button' id="{{$category->category_name}}" data-toggle="pill" href="#pills-{{$category->category_name}}" role="tab" aria-controls="pills-{{$category->category_name}}" aria-selected="false">{{str_replace('_',' ',$category->category_name)}}</a>
                            </li>
                            @else
                            <li class="nav-item">
                            <a class="nav-link btn btn-outline-info" type='button' id="{{$category->category_name}}" data-toggle="pill" href="#pills-{{$category->category_name}}" role="tab" aria-controls="pills-{{$category->category_name}}" aria-selected="false">{{str_replace('_',' ',$category->category_name)}}</a>
                            </li>
                            @endif
                            @endforeach
                       </ul>
                    </div>
                 </div>   

                <div class='row'>
                    <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                        <hr class="my-4">
                    </div>  
                </div>
            </div>
               
            <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
             <div class="tab-content" id="pills-tabContent">
                 @foreach($categories as $category)
                        @if($category->category_name == 'washing')
                <div class="tab-pane fade show active" id="pills-{{$category->category_name}}" role="tabpanel" aria-labelledby="pills-profile-tab">
                	<div class='row'>
                    @foreach($products as $product)
                         @if($product->category_id == 1)
                    <div class='col-12 col-sm-12 col-md-6 col-lg-6 paddin-bottom'>
                        <div class='row'>
                            <div class='col-12 col-sm-12 col-md-7 col-lg-7'>
				        <div class='product-center-for-responsive-design'>
                                @php
                                    $data = getimagesize($product->image);
                                    $width = $data[0];
                                @endphp
                                @if($width >= 200 && $width <= 220)    
									<img class='img-fluid product-left-margin' src='{{asset($product->image)}}' alt='{{$product->pro_name}}' />
                                @elseif($width >= 221 && $width <= 250)    
									<img class='img-fluid product-left-margin-250' src='{{asset($product->image)}}' alt='{{$product->pro_name}}' />
                                @else  
                                    <img class='img-fluid alignment' src='{{asset($product->image)}}' alt='{{$product->pro_name}}' />
                                @endif    
								</div>
                            </div>
                            <div class='col-12 col-sm-12 col-md-5 col-lg-5'>
								<div>
									<p class='pro_name'>{{$product->pro_name}}</p>
                                    @if(!empty($product->sub_pro_name))
                                    <p class='pro_name_sub'>{{$product->sub_pro_name}}</p>
                                    @endif
									@if(!empty($product->brand))
                                    <div class='row'>
                                        <div class='col-5 brand_name'>
                                            <p>Manufacturer<span class='text-white ml-2'>:</span></p>
                                        </div>
                                        <div class='col-7 brand_name-1'>
                                            <p>{{strtoupper($product->brand)}}</p>
                                        </div>
                                    </div>
                                    @else
                                    <div class='row'>
                                        <div class='col-12 brand_name_null'>
                                        <p class='toltip'>Manufacturer :&nbsp;{{"Inquiry"}}
                                        <span class="tooltiptext">Please email to 'fazlul@deltapharmabd.com'</span>
                                       </p>
                                        </div>
                                    </div>
                                    @endif
									<p class='origin_name'>Origin:&nbsp;{{strtoupper($product->origin)}}</p>
									@if($product->url !='')
                                    <div class='row'>
                                    <div class='col-6 text-center'>
									<a href='{{$product->url}}' target='_blank' class='btn btn-outline-info'>Details</a>
                                    </div>  
                                    <div class='col-6 text-center'>
                                    <a href='{{$product->url}}' target='_blank' class='btn btn-outline-info'>Client List</a>
                                    </div>
                                    </div>
									@else
                                    <div class='row'>
                                    <div class='col-6 text-center'>
                                     <p class='btn btn-outline-light' >Details</p>
                                    </div>
                                    <div class='col-6 text-center'>
                                    <p class='btn btn-outline-light' >Client List</p>
                                    </div>  
                                    </div> 
									@endif
								</div>
                            </div>
                        </div>
                    </div> 
                       @endif
                    @endforeach   
                </div>
            </div>
              @else   
            <div class="tab-pane fade" id="pills-{{$category->category_name}}" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div class='row'>
                    @foreach($products as $product)
                    @if($product->category_id == $product->category->id && $product->category->category_name != 'washing' && $product->category->category_name == $category->category_name)
                    <div class='col-12 col-sm-12 col-md-6 col-lg-6 paddin-bottom'>
                        <div class='row'>
                            <div class='col-12 col-sm-12 col-md-7 col-lg-7'>
                                <div class='product-center-for-responsive-design'>
                                 @php
                                    $data = getimagesize($product->image);
                                    $width = $data[0];
                                @endphp
                                @if($width >= 200 && $width <= 220)    
									<img class='img-fluid product-left-margin' src='{{asset($product->image)}}' alt='{{$product->pro_name}}' />
                                @elseif($width >= 221 && $width <= 250)    
									<img class='img-fluid product-left-margin-250' src='{{asset($product->image)}}' alt='{{$product->pro_name}}' />
                                @else  
                                    <img class='img-fluid alignment' src='{{asset($product->image)}}' alt='{{$product->pro_name}}' />
                                @endif    
								</div>	
                            </div>
                            <div class='col-12 col-sm-12 col-md-5 col-lg-5'>
								<div>
                                <p class='pro_name'>{{$product->pro_name}}</p>
                                    @if(!empty($product->sub_pro_name))
                                    <p class='pro_name_sub'>{{$product->sub_pro_name}}</p>
                                    @endif
                                @if(!empty($product->brand))
                                    <div class='row'>
                                        <div class='col-5 brand_name'>
                                            <p>Manufacturer<span class='text-white ml-2'>:</span></p>
                                        </div>
                                        <div class='col-7 brand_name-1'>
                                            <p>{{strtoupper($product->brand)}}</p>
                                        </div>
                                    </div>
                                    @else
                                    <div class='row'>
                                        <div class='col-12 brand_name_null'>
                                        <p class='toltip'>Manufacturer :&nbsp;{{"Inquiry"}}
                                        <span class="tooltiptext">Please email to 'fazlul@deltapharmabd.com'</span>
                                       </p>
                                        </div>
                                    </div>
                                 @endif  
                                    
                                <p class='origin_name'>Origin:&nbsp;{{strtoupper($product->origin)}}</p>
                                @if($product->url !='')
                                 <div class='row'>
                                  <div class='col-6 text-center'>
                                <a href='{{$product->url}}' target='_blank' class='btn btn-outline-info'>Details</a>
                                  </div>
                                <div class='col-6 text-center'>
                                <a href='{{$product->url}}' target='_blank' class='btn btn-outline-info'>Client List</a>
                                </div>
                                </div>    
                                @else
                                <div class='row'>
                                    <div class='col-6 text-center'>
                                     <p class='btn btn-outline-light' >Details</p>
                                    </div>
                                    <div class='col-6 text-center'>
                                    <p class='btn btn-outline-light' >Client List</p>
                                    </div>  
                                </div>    
                                @endif
								</div>	
                            </div>
                        </div>
                    </div> 
                    @endif
                    @endforeach   
                </div>
            </div>
             @endif     
            @endforeach         
            </div>
           </div>
        </div>
    </div>
</section>

@endsection