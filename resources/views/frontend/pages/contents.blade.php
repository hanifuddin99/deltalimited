@extends('frontend.layouts.master')
@section('title','About Us')
@section('content')
<!-- banner -->
<div class="container">
    <div class='row'>
        <div class='col-12'>
            <div class='img-container'>
                <img src='{{asset('uploads/contents/'.$contents->image)}}' class='img-fluid' alt='' />
                <h1 class='bottom-left'>
                    @php
                     $space=array('_');
                     $a = $contents->title;
                     if(preg_match('|_|',$a)){
                       $name = str_replace($space,' ', $a);
                       echo ucwords($name);
                     }else{
                       echo ucwords($a);
                    }
                    @endphp
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- /banner -->
<div class='container'>
    <div class='row'>
        <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
               {!! $contents->description !!}
        </div>
    </div>
</div>

@endsection