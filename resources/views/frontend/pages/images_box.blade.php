@extends('frontend.layouts.master')
@section('content')
<!-- banner -->
<div class="about_bnr">
    <div class="container"></div>
</div>
<!-- /banner -->
<div class='container'>
    <div class='row'>
        <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                <h1 class='text-center text-muted'>Albums</h1>
                <hr />
        </div>
        @foreach($images as $image)
        <div class='col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3'>
            <a href="{{route('photo_details',['id' =>$image->id])}}">
            <div class="polaroid">
                  <img src="{{asset('/uploads/albums/'.$image->image)}}" alt="" class='img-fluid'>
                  <div class="text-container">
                        <p class='text-uppercase'>{{$image->album_name}}</p>
                  </div>
            </div>
            </a>
        </div>
        @endforeach
    </div>
</div>

@endsection