@extends('frontend.layouts.master')
@section('content')

<div class="about_bnr">
    <div class="container"></div>
</div>
<!-- /banner -->
<div class='container' id='video'>
        <div class='row'>
            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                <h1 class='text-center'>Videos</h1>
                <hr />
            </div>
        </div>  
    <div class='scrollbarr'>
    <div class='container' id='video'>
        @foreach($videos as $video)
        <div class='row video-bottom-margin'>
            <div class='col-md-2 col-lg-2 col-xl-2'></div>
            <div class='col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4'>
                <iframe class='video-width-height'  src="{{$video->video_url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class='col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4'>
                    <p class='font-for-video'>
                        {{$video->video_text}}
                    </p>
            </div>
            <div class='col-md-2 col-lg-2 col-xl-2'></div>
        </div>
        @endforeach
    </div>
    </div>
</div>
@endsection
