@extends('frontend.layouts.master')
@section('title','Home')
@section('content')
    <section id='carousel-part' class='carousel-divv' style="z-index:1">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
                      </ol>
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img class="d-block w-100" src="{{asset('/assets/products_image/2018-11-13-07-00-38.png')}}" alt="">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" src="{{asset('/assets/products_image/banner-2-bio.jpg')}}" alt="">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" src="{{asset('/assets/products_image/2018-11-13-07-04-49.jpg')}}" alt="">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" src="{{asset('/assets/products_image/banner-1-echo.jpg')}}" alt="Fourth slide">
                        </div>
                        <div class="carousel-item">
                          <img class="d-block w-100" src="{{asset('/assets/products_image/Providing_Health_Solution.jpg')}}" alt="Fifth slide">
                        </div>
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>
         </section>
        <section id='news'>
            <div class='container-fluid'>
                <div class='row'>
                    <div class='col-12 col-sm-12 col-md-1 col-lg-1 news-background news-border-left'>
                    </div>
                    <div class='col-12 col-sm-12 col-md-10 col-lg-10 marque-margin'>
                        <marquee behaviour='scroll' direction='left' onmouseover='this.stop();' onmouseout='this.start();' >
                            <a href=''>
                                Welcome To Delta Limited.The Latest Service for Medical Sector.
                            </a>&nbsp;
                        </marquee>
                    </div>
                    <div class='col-12 col-sm-12 col-md-1 col-lg-1 news-background news-border-right'>
                    </div>
                </div>
            </div>
        </section>
        <section id='partner-id' class='partner-content'>
              <div class='jumbotron'>
                     <div class='row distance-clients-top'>
                          <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
                            <h1 class='heading-for-partner'>Our Valuable Clients</h1>
                          </div>
                        <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
                            <div id='carouselExampleControls' class='carousel slide' data-ride='carousel'>
                                <div class='carousel-inner'>
                                    <div class='carousel-item active'>
                                        <div class='row'>
                                            @for($i = 0; $i < 3; $i++)
                                                <div class="col-sm">
                                                    <a href='{{$clients[$i]['url']}}' target='_blank'>
                                                    <img class="d-block w-100" src="{{asset($clients[$i]['image'])}}" alt="First slide">
                                                    </a>    
                                                </div>
                                            @endfor
                                             </div>
                                      </div>
                                        <div class='carousel-item'>
                                            <div class='row'>
                                               @for($i; $i < 6; $i++)
                                                <div class="col-sm">
                                                    <a href='{{$clients[$i]['url']}}' target='_blank'>
                                                    <img class="d-block w-100" src="{{asset($clients[$i]['image'])}}" alt="First slide">
                                                    </a>    
                                                </div>
                                            @endfor
                                            </div>
                                        </div>  
                                        <div class='carousel-item'>
                                            <div class='row'>
                                               @for($i; $i < 9; $i++)
                                                <div class="col-sm">
                                                    <a href='{{$clients[$i]['url']}}' target='_blank'>
                                                    <img class="d-block w-100" src="{{asset($clients[$i]['image'])}}" alt="First slide">
                                                    </a>    
                                                </div>
                                            @endfor
                                            </div>
                                        </div>  
                                        <div class='carousel-item'>
                                            <div class='row'>
                                                @php    
                                                    $j=0;
                                                @endphp
                                               @for($i; $i < $total_clients_row; $i++)
                                                <div class="col-sm">
                                                    <a href='{{$clients[$i]['url']}}' target='_blank'>
                                                    <img class="d-block w-100" src="{{asset($clients[$i]['image'])}}" alt="First slide">
                                                    </a>    
                                                </div>
                                                  @php    
                                                    $j++;
                                                  @endphp 
                                                @endfor
                                                @if($j ==1 || $j == 2)
                                                 @for($i=0; $i < 1; $i++)
                                                  <div class="col-sm">
                                                      
                                                 </div>                    
                                                 @endfor                       
                                               @endif
                                            </div>
                                        </div>    
                                    </div>
                              <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <i class="carousel-control-left-icon" aria-hidden='true'></i>
                                <i class='sr-only'>Previous</i>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                  <i class='carousel-control-right-icon' aria-hidden='true'></i>
                                <i class='sr-only'>Next</i>
                              </a>
                             </div>
                        </div>
                         
                    </div>
                </div>
        </section>
        <div class='container background-upon-of-projects'>
            <div class='row'>
                <div class='col-12 col-sm-12 col-md-2 col-lg-2'>
                   
                </div>
                <div class='col-12 col-sm-12 col-md-8 col-lg-8'>
                  <hr />  
                </div>
                <div class='col-12 col-sm-12 col-md-2 col-lg-2'>
                    
                </div>
            </div>
        </div>

        <section id='item-content-id' class='item-content'>
            <div class='container'>
                <div class='row'>
                    <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
                            <h1 class='heading-for-partner'>Our Sister Concerns</h1>
                     </div>
                    <div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='http://deltapharmabd.com/' target='_blank'><img    src='{{url("/assets/products_image/deltapharma.jpg")}}'></a> 
								</div>
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
									<a class='healthcare-text-box' href='http://deltapharmabd.com/' target='_blank'>Delta Pharma Ltd.</a>
                            </div>
                        </div>
                    </div>
                    <div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='http://deltapharmabd.com/api_concern.php' target='_blank'><img    src='{{url("/assets/products_image/deltaapi_client_logo.jpg")}}'></a> 
								</div>
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
									<a class='healthcare-text-box' href='http://deltapharmabd.com/api_concern.php' target='_blank'>Delta API Ltd.</a>
                            </div>
                        </div>
                    </div>
                    <div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding text-center'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='https://www.facebook.com/Delta-Health-Care-Mirpur-Ltd-399281826911279/' target='_blank'><img    src='{{url("/assets/products_image/mirpur_healthcare.png")}}'></a> 
								</div>
                                 
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 text-center'>
									<a class='healthcare-text-box' href='https://www.facebook.com/Delta-Health-Care-Mirpur-Ltd-399281826911279/' target='_blank'>DHC Mirpur Ltd.</a>
                            </div>
                        </div>
                    </div><div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding text-center'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='https://www.facebook.com/Delta-Health-Care-Jatrabari-Ltd-1527719244205005/' target='_blank'><img    src='{{url("/assets/products_image/jatrabari_healthcare.png")}}'></a> 
								</div>
                                 
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 text-center'>
									<a class='healthcare-text-box' href='https://www.facebook.com/Delta-Health-Care-Jatrabari-Ltd-1527719244205005/' target='_blank'>DHC Jatrabari Ltd.</a>
                            </div>
                        </div>
                    </div><div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding text-center'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='https://www.facebook.com/Delta-Health-Care-Rampura-Ltd-1552519145009177/' target='_blank'><img    src='{{url("/assets/products_image/rampura_healthcare.png")}}'></a> 
								</div>
                                 
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 text-center'>
									<a class='healthcare-text-box' href='https://www.facebook.com/Delta-Health-Care-Rampura-Ltd-1552519145009177/' target='_blank'>DHC Rampura Ltd.</a>
                            </div>
                        </div>
                    </div><div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding text-center'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='https://www.facebook.com/Delta-Health-Care-Mymensingh-Limited-878713212221869/' target='_blank'><img    src='{{url("/assets/products_image/mymensingh_healthcare.png")}}'></a> 
								</div>
                                 
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 text-center'>
									<a class='healthcare-text-box' href='https://www.facebook.com/Delta-Health-Care-Mymensingh-Limited-878713212221869/' target='_blank'>DHC Mymensingh Ltd.</a>
                            </div>
                        </div>
                    </div><div class='col-12 col-sm-12 col-md-2 col-lg-2 healthcare-box-bottom-padding text-center'>
                        <div class='row'>
                            <div class='wow fadeInLeft col-12 col-sm-12 col-md-12 col-lg-12 '  data-wow-offset='200' data-wow-duration='0.4s'>
								<div class='healthcare-img-box'>
									<a   href='http://deltahealthcarectg.com/' target='_blank'><img    src='{{url("/assets/products_image/chittagong_healthcare.png")}}'></a> 
								</div>
                                 
                            </div>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12 text-center'>
									<a class='healthcare-text-box' href='http://deltahealthcarectg.com/' target='_blank'>DHC Chittagong Ltd.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class='container'>
            <div class='row'>
                <div class='col-12 col-sm-12 col-md-12 col-lg-12 padding-for-footer'>
                </div>
            </div>
        </div>
@endsection