@extends('frontend.layouts.master')
@section('content')
<!-- banner -->
<div class="about_bnr">
    <div class="container"></div>
</div>
<!-- /banner -->
<div class='container'>
    <div class='row'>
        <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                <h1 class='text-center'>News And Events</h1>
                <hr />
        </div>
        <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
            <div class='news-img-box'>
                <div id="carouselNewsIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
              @php
                $i=0;
              @endphp
              @for($i; $i < $total_news_row ; $i++)
            <li data-target="#carouselNewsIndicators" data-slide-to="{{$i}}" class="{{$i==0 ? 'active' : ''}}">
            </li>
              @endfor
          </ol>
              @php
                $i=0;
              @endphp
              <div class="carousel-inner">
                 @foreach($news as $news)
                <div class="carousel-item {{$i==0 ? 'active' : ''}}">
                    <a href='{{asset($news->image)}}' data-fancybox="images">
                  <img class="d-block w-100" src="{{asset($news->image)}}" alt="">
                    </a>
                </div>
                {{$i++}}
                @endforeach  
              </div>
          <a class="carousel-control-prev" href="#carouselNewsIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselNewsIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
            </div>
        </div>
            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                <h5 class='lined'>Recent Events</h5>
            </div>        
            @foreach($news1 as $news)
            <div class='col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4'>
                <div class='image-box'>
                    <a href='{{asset($news->image)}}' data-fancybox="images">
                       <img src="{{asset($news->image)}}" class='img-fluid' alt=""  />
                    </a>
                </div>
            </div>
             @endforeach
    <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
               <br />
               <br />
    </div>  
    </div>
</div>
@endsection