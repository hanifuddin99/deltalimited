@extends('frontend.layouts.master')
@section('content')
<!-- banner -->
<div class="about_bnr">
    <div class="container"></div>
</div>
<!-- /banner -->
<div class='container'>
    <div class='row'>
        <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                <h1 class='text-center'>Contact</h1>
                <hr />
        </div>
        <div class='col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6'>
            <div class="mapouter">
                <div class="gmap_canvas">
                    <iframe class='google_map_width_height' id="gmap_canvas" src="https://maps.google.com/maps?q=H%20No%20%3A%20503(A)%2CRd%20No%20%3A%2034%2C%20New%20DOHS%20Mohakhali%2C%20Dhaka&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    <a href="https://www.embedgooglemap.net"></a>
                </div>
            </div>
        </div>
        <div class='col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6'>
            <div><h5 class='lined'>&nbsp;&nbsp;Contact Info</h5></div>
            <div class='contact-info'>
                <i class="fa fa-home" aria-hidden="true"></i><span>&nbsp;&nbsp;House No : 503(A)</span><br/>
                <i class="fa fa-road" aria-hidden="true"></i><span>&nbsp;&nbsp;Road No : 34</span><br/>
                <i class="fa fa-cloud" aria-hidden="true"></i><span>&nbsp;&nbsp;New DOHS Mohakhali, Dhaka</span><br/>
                <i class="fa fa-phone" aria-hidden="true"></i><span>&nbsp;&nbsp;+880 2-9892192</span><br/>
                <i class="fa fa-mobile fa-lg" aria-hidden="true"></i><span>&nbsp;&nbsp;+880 17555-98205</span><br/>
                <i class="fa fa-envelope" aria-hidden="true"></i><span>&nbsp;&nbsp;fazlul@deltapharmabd.com </span>
            </div>
            <div>
                <p></p>
            </div>
            <div><h5 class='lined'>&nbsp;&nbsp;Follow Us</h5></div>
            <div class="follow-us">
                  <a href="https://www.facebook.com/Delta-Limited-304473997086339/" target='_blank'><i class="fa fa-2x fa-facebook-square"></i></a>
                  <a href="https://www.youtube.com/watch?v=4yt3XKCfaoE" target="_blank"><i class="fa fa-2x fa-youtube-square"></i></a>
            </div>
        </div>
        <div>
            <br />
            <br />
        </div>
    </div>
</div>
@endsection