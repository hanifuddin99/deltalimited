@extends('frontend.layouts.master')
@section('content')
<!-- banner -->
<div class="about_bnr">
    <div class="container"></div>
</div>
<!-- /banner -->
    <div class='container' id='image'>
        <div class='row'>
            <div class='col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12'>
                <h1 class='text-center text-muted'>Images</h1>
                <hr />
            </div>
             @foreach($images as $image)
            <div class='col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4'>
                <div class='image-box'>
                    <a href='{{asset("/uploads/images/".$image->image)}}' data-fancybox="images" data-caption='<p class="img-caption">{{$image->name}}</p>'>
                       <img src="{{asset('/uploads/images/'.$image->image)}}" class='img-fluid' alt=""  />
                    </a>
                </div>
            </div>
             @endforeach
        </div>
    </div>
@endsection