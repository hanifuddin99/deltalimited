@extends('frontend.layouts.master')
@section('content')
<!-- banner -->
<div class="container">
    <div class='row'>
        <div class='col-12'>
            <div class='img-container' style="border:20px solid #F8931D;border-radius:20px;">
                <img src='{{asset('uploads/contents/'.$content->image)}}' class='img-thumbnail' alt='' />
                <h1 class='bottom-left'>
                    @php
                     $space=array('_');
                     $a = $content->title;
                     if(preg_match('|_|',$a)){
                       $name = str_replace($space,' ', $a);
                       echo ucwords($name);
                     }else{
                       echo ucwords($a);
                    }
                    @endphp
                </h1>
            </div>
        </div>
    </div>
</div>
<!-- /banner -->

<div class='container'><div clas='row'>
     <div class='h-100 d-inline-block'>
    </div>
</div></div>
<div class='container'>
    <div class='row'>
<div class='custom_img_container'>
        @foreach($directors as $director)
        <div class='polaroid'>
               <a href='#' class='director' data-toggle="modal" data-name="{{$director->description}}" data-target="#directorModal">
                  <img src="{{asset($director->image)}}" alt="" class='img-fluid'>
                  <div class="text-container">
                        <strong class='director-text'>{{ucwords($director->director_name)}}</strong>
                        <p class='director-text'>{{ucwords($director->designation)}}</p>
                  </div>
                </a>
        </div>
		@endforeach
</div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="directorModal" tabindex="-1" role="dialog" aria-labelledby="directorModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="directorModal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class='col-12'>
					
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection