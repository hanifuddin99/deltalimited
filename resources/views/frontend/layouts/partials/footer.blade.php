    
    <section class='footer1-bg'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <h3 class="text-center color mt-1">Emergency Contact</h3>
                        <hr/>
                        <i class="fa fa-home color" aria-hidden="true"></i><span class='color'>&nbsp;&nbsp;House No : 503(A)</span><br/>
                        <i class="fa fa-road color" aria-hidden="true"></i><span class='color'>&nbsp;&nbsp;Road No : 34</span><br/>
                        <i class="fa fa-cloud color" aria-hidden="true"></i><span class='color'>&nbsp;&nbsp;New DOHS Mohakhali, Dhaka</span><br/>
                        <i class="fa fa-phone color" aria-hidden="true"></i><span class='color'>&nbsp;&nbsp;+880 2-9892192</span><br/>
                        <i class="fa fa-mobile fa-lg color" aria-hidden="true"></i><span class='color'>&nbsp;&nbsp;+880 17555-98205</span><br/>
                        <i class="fa fa-envelope color" aria-hidden="true"></i><span class='color'>&nbsp;&nbsp;fazlul@deltapharmabd.com </span><br/>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <h3 class="text-center color mt-1">Our Sister Concerns</h3>
                        <hr/>
                        <ul class="list-stylle quick_link">
                            <li><a href="http://www.deltapharmabd.com/" target='_blank' class="color">Delta Pharma Ltd.</a></li>
                            <li><a href="http://www.deltapharmabd.com/api_concern.php" target='_blank' class="color">Delta API Ltd.</a></li>
                            <li><a href="https://www.facebook.com/Delta-Health-Care-Mirpur-Ltd-399281826911279/" target='_blank' class="color">DHC Mirpur Ltd.</a></li>
                            <li><a href="https://www.facebook.com/Delta-Health-Care-Jatrabari-Ltd-1527719244205005/" target='_blank' class="color">DHC Jatrabari Ltd.</a></li>
                            <li><a href="https://www.facebook.com/Delta-Health-Care-Rampura-Ltd-1552519145009177/" target='_blank' class="color">DHC Rampura Ltd.</a></li>
                            <li><a href="https://www.facebook.com/Delta-Health-Care-Mymensingh-Limited-878713212221869/" target='_blank' class="color" target="_blank">DHC Mymensingh Ltd.</a></li>
                            <li><a href="http://deltahealthcarectg.com" target="_blank" target='_blank' class="color">DHC Chittagong Ltd.</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <h3 class="color footer-header-center-for-normal mt-1">Quick Link</h3>
                        <h3 class="text-center color footer-header-center-for-responsive">Quick Link</h3>
                        <hr/>
                        <ul class="quick_link list-stylle">
                            <li><a href="http://www.atlanticchemicals.com/" target='_blank' class="color">About Chemicals</a></li>
                            <li><a href="http://www.imesa.it/en/" target='_blank' class="color">Washing Machine</a></li>
                            <li><a href="http://www.comenda.eu/asp/default.asp?lng=eng" target='_blank' class="color">Dish Washing</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <h3 class="color footer-header-center-for-normal mt-1">Follow Us</h3>
                        <h3 class="text-center color footer-header-center-for-responsive">Follow Us</h3>
                        <hr />
                        <div class="text-center">
                            <a href="https://www.facebook.com/Delta-Limited-304473997086339/" target='_blank'><i class="fa fa-2x fa-facebook-square color-social"></i></a>
                            <a href="https://www.youtube.com/watch?v=4yt3XKCfaoE" target="_blank"><i class="fa fa-2x fa-youtube-square color-social"></i></a>
                        </div>
                        <form>
                            <div class="form-group">
                                <input type="button" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="button" class="btn btn-success btn-block">
                            </div>
                        </form>   
                    </div>
                </div>
            </div>
        </section>

        <section class='footer2-bg'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-center color-bg-2" style="padding-top: 20px;">Copyright@ Delta Limited-2019</p>
                    </div>
                </div>
            </div>
        </section>