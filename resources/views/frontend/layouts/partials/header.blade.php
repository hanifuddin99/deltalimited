<section>
@php
function getBrowser() { 
  $u_agent = $_SERVER['HTTP_USER_AGENT'];
  $bname = 'Unknown';
  $platform = 'Unknown';
  $version= "";

  //First get the platform?
  if (preg_match('/linux/i', $u_agent)) {
    $platform = 'linux';
  }elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
    $platform = 'mac';
  }elseif (preg_match('/windows|win32/i', $u_agent)) {
    $platform = 'windows';
  }

  // Next get the name of the useragent yes seperately and for good reason
  if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)){
    $bname = 'Internet Explorer';
    $ub = "MSIE";
  }elseif(preg_match('/Firefox/i',$u_agent)){
    $bname = 'Mozilla Firefox';
    $ub = "Firefox";
  }elseif(preg_match('/OPR/i',$u_agent)){
    $bname = 'Opera';
    $ub = "Opera";
  }elseif(preg_match('/Chrome/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
    $bname = 'Google Chrome';
    $ub = "Chrome";
  }elseif(preg_match('/Safari/i',$u_agent) && !preg_match('/Edge/i',$u_agent)){
    $bname = 'Apple Safari';
    $ub = "Safari";
  }elseif(preg_match('/Netscape/i',$u_agent)){
    $bname = 'Netscape';
    $ub = "Netscape";
  }elseif(preg_match('/Edge/i',$u_agent)){
    $bname = 'Edge';
    $ub = "Edge";
  }elseif(preg_match('/Trident/i',$u_agent)){
    $bname = 'Internet Explorer';
    $ub = "MSIE";
  }

  // finally get the correct version number
  $known = array('Version', $ub, 'other');
  $pattern = '#(?<browser>' . join('|', $known) .
')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
  if (!preg_match_all($pattern, $u_agent, $matches)) {
    // we have no matching number just continue
  }
  // see how many we have
  $i = count($matches['browser']);
  if ($i != 1) {
    //we will have two since we are not using 'other' argument yet
    //see if version is before or after the name
    if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
        $version= $matches['version'][0];
    }
  }else {
    $version= $matches['version'][0];
  }

  // check if we have a number
  if ($version==null || $version=="") {$version="?";}

  return array(
    'userAgent' => $u_agent,
    'name'      => $bname,
    'version'   => $version,
    'platform'  => $platform,
    'pattern'    => $pattern
  );
} 

// now try it   
$ua=getBrowser();
$yourbrowser=  $ua['name'] . " " . $ua['version'];
if($yourbrowser == 'Internet Explorer 9.0'){
	$warning_text = "Please Update your Browser atleast upto Internet explorer 10";
}elseif($yourbrowser == 'Internet Explorer 8.0'){
	$warning_text = "Please Update your Browser atleast upto Internet explorer 10";
}elseif($yourbrowser == 'Internet Explorer 7.0'){
	$warning_text = "Please Update your Browser atleast upto Internet explorer 10";
}elseif($yourbrowser == 'Internet Explorer 6.0'){
	$warning_text = "Please Update your Browser atleast upto Internet explorer 10";
}

@endphp
    @if(!empty($warning_text))
    <div class=container>
        <div class='row'>
          <div class='col-12'>
              <p class='warning_text'>
                  @php
                    echo $warning_text;
                  @endphp
              </p>
            </div>
        </div>
    </div>
    @endif
</section>
    @php
    $menu_active=Request::path();
    if(preg_match('|messages/|',$menu_active)){
     $menu_active='about';
    }elseif(preg_match('|products/|',$menu_active)){
     $menu_active='products';    
    }elseif(preg_match('|photo_details/|',$menu_active)){
     $menu_active='photos';    
    }
   
    @endphp
        <section class='top-background-2 {{Request::path() == "/" ? "index" : $menu_active}}'>
         <nav class="navbar fixed-top navbar-expand-md flex-nowrap navbar-new-top">
            <a class="navbar-brand" href="{{route('/')}}">
              <img src="{{asset('/assets/products_image/image-20180220172110-1.jpeg')}}">
            </a>
             
            <ul class="nav navbar-nav mr-auto"></ul>
             <form class='form-inline my-2 my-log-0 search-disable'>
                <input class='form-control mr-sm-2 search-width' type='search' placeholder='Search' aria-label='Search'>
                 <button class='btn btn-outline-success my-2 my-sm-0' type='submit'>Search</button>
             </form>
             <ul class="nav navbar-nav mr-auto"></ul>
            <ul class="navbar-nav flex-row">
<!--
                <li class="nav-item">
                    <a class="nav-link px-2">Link</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link px-2">Link</a>
                </li>
-->
                
                <li class='nav-item mail'>
                     <a class="nav-link px-2" target='_blank' href='{{url("http://mail.deltapharmabd.com/")}}'>
                        <i class=" fa fa-envelope-open-o fa-lg"></i>
                     </a>
                </li>
            </ul>
        </nav>
        <nav class="navbar fixed-top navbar-expand-md navbar-light bg-light navbar-new-bottom" >
                  
                  <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                 <span class="navbar-toggler-icon"></span>
                  </button>
            
                  <div class="collapse navbar-collapse pt-2 pt-md-0" id="navbarNavDropdown">
                    <ul class="navbar-nav ul-responsive-background only-nav-unordered-list-color w-100 justify-content-center px-3">
                        
                      @foreach($navmenus as $navmenu)
                        @if($navmenu->has_subnavmenu == 0)
                        @if(!empty($navmenu->navmenu_url))
                        <li class="nav-item">
                        <a class="nav-link" href="{{route($navmenu->navmenu_url)}}">
                            @php 
                                $menu_name = ucwords(str_replace('_',' ',$navmenu->menu_name));
                            @endphp
                            {{$menu_name}}
                        </a>
                         </li>
                        @endif  
                        @else
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            @php 
                                $menu_name = ucwords(str_replace('_',' ',$navmenu->menu_name));
                            @endphp
                            {{$menu_name}}
                        </a>
                          <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                          @foreach($subnavmenus as $subnavmenu)
                            @if($subnavmenu->navmenu_id == $navmenu->id)
                            <li class="dropdown-submenu">
                                @if($subnavmenu->subnavmenus_url != NULL)
                                @if($subnavmenu->subnavmenus_url == 'photos')
                                <a class="dropdown-item {{$subnavmenu->has_subnavmenus==1 ? 'dropdown-toggle':''}}" href="{{route("photos")}}">
                                @elseif($subnavmenu->subnavmenus_url == 'videos')
                                <a class="dropdown-item {{$subnavmenu->has_subnavmenus==1 ? 'dropdown-toggle':''}}" href="{{route("videos")}}">
                                @else
                                <a class="dropdown-item {{$subnavmenu->has_subnavmenus==1 ? 'dropdown-toggle':''}}" href="{{route('messages',['name'=>$subnavmenu->subnavmenus_url])}}">
                                @endif
                                @else
                                 <a class="dropdown-item {{$subnavmenu->has_subnavmenus==1 ? 'dropdown-toggle':'#'}}" href="#">   
                                @endif
                                @php 
                                $subnavmenu_name = ucwords(str_replace('_',' ',$subnavmenu->subnavmenu_name));
                                @endphp
                                    {{$subnavmenu_name}}
                                </a>
                                 @if($subnavmenu->has_subnavmenus == 1) 
                                    @if($subnavmenu->subnavmenu_name=='industrial')
                                 <ul class="dropdown-menu">
                                    @foreach($subsubnavmenus1 as $subsubnavmenu)
                                    <li>
                                  <a class="dropdown-item" href="{{route('products',['id'=>$subsubnavmenu->category_id])}}">
                                      @php 
                                $subsubnavmenu_name = ucwords(str_replace('_',' ',$subsubnavmenu->subsubnavmenu_name));
                                @endphp
                                     {{$subsubnavmenu_name}}
                                  </a>
                                </li>
                                   @endforeach
                               </ul>         
                                @elseif($subnavmenu->subnavmenu_name=='medical')
                                    <ul class="dropdown-menu">
                                    @foreach($subsubnavmenus as $subsubnavmenu)
                                        @if($subsubnavmenu->subsubnavmenu_name == 'medical')
                                        @php
                                        break
                                        @endphp
                                        @else
                                    <li>
                                  <a class="dropdown-item" href="{{route('products',['id'=>$subsubnavmenu->category_id])}}">
                                      @php 
                                $subsubnavmenu_name = ucwords(str_replace('_',' ',$subsubnavmenu->subsubnavmenu_name));
                                if($subsubnavmenu->subsubnavmenu_name == 'cssd'){
                                        $subsubnavmenu_name = strtoupper($subsubnavmenu->subsubnavmenu_name);
                                      }
                                    @endphp
                                     {{$subsubnavmenu_name}}
                                  </a>
                                </li>   
                                        @endif
                                      @endforeach
                                </ul>
                                @elseif($subnavmenu->subnavmenu_name=='all_products')    
                                    <ul class="dropdown-menu">
                                    @foreach($subsubnavmenus2 as $subsubnavmenu)
                                    <li>
                                  <a class="dropdown-item" href="{{route('products',['id'=>$subsubnavmenu->subsubnavmenu_name])}}">
                                      @php 
                                $subsubnavmenu_name = ucwords(str_replace('_',' ',$subsubnavmenu->subsubnavmenu_name));
                                if($subsubnavmenu->subsubnavmenu_name == 'cssd'){
                                        $subsubnavmenu_name = strtoupper($subsubnavmenu->subsubnavmenu_name);
                                      }
                                    @endphp
                                     {{$subsubnavmenu_name}}
                                  </a>
                                </li>
                                      @endforeach
                                </ul>
                                  @endif
                                @endif    
                              </li>
                                @endif
                              @endforeach
                            </ul>
                            </li>    
                         @endif
                        @endforeach
                      </ul>            
                        <ul class="navbar-nav ul-responsive-background only-nav-unordered-list-color w-100 justify-content-center px-3 search-width-responsive">
                       <form class='form-inline my-2 my-log-0'>
                        <input class='form-control mr-sm-2 search-width' type='search' placeholder='Search' aria-label='Search'>
                         <button class='btn btn-outline-success my-2 my-sm-0' type='submit'>Search</button>
                      </form>
                    </ul>
                  </div>
                </nav>
         </section>
         <section id='top' class='top-background-3'>
                <div class='container-fluid'>
                        <div class='row top-height-3'>
                            <div class='col-12 col-sm-12 col-md-12 col-lg-12'>
                            </div>
                       </div>
                </div>
         </section>


