<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{config('app.name')}} | @yield('title')</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Arimo:700" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.css" />    
        <link rel='stylesheet' href='{{asset("/assets/front_end/css/mystyle.css")}}?time={{time()}}' type='text/css'>
    </head>
    <body>
        <!--  header -->
        @include('frontend.layouts.partials.header')
        <!-- /header -->
        
        <!-- content -->
            @yield('content')
        <!-- /content-->
        
        <!--  footer -->
        @include('frontend.layouts.partials.footer')
        <!-- /footer -->
        
         <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>

        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.6/jquery.fancybox.min.js"></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js'></script>
        <script>
            $(document).ready(function(){
                
              // animate effect initialize
                new WOW().init();
             // active class for navigation menu
            $(".index a:contains('Home')").parent().addClass("active");
            $(".about  a:contains('About Us')").parent().addClass("active");
            $(".products a:contains('Products')").parent().addClass("active");
            $('.photos a:contains("Gallery")').parent().addClass('active');
            $('.videos a:contains("Gallery")').parent().addClass('active');
            $('.contact a:contains("Contact")').parent().addClass('active');        
            // Javascript to enable link to tab
            /*var url = document.location.toString();
            if (url.match('#')) {
            $('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
            } 
            // Change hash for page-reload
            $('.nav-pills a').on('shown.bs.tab', function (e) {
               window.location.hash = e.target.hash;
            })*/
            
            $('.director').each(function(){
            $(this).click(function(e){
             let description = $(this).data('name');
                $('.modal-body').html(description);
            });
            });    
            });
        </script>
    </body>
</html>
