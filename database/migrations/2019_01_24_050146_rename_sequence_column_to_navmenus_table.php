<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameSequenceColumnToNavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('navmenus', function (Blueprint $table) {
            $table->renameColumn('sequence', 'has_subnavmenu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('navmenus', function (Blueprint $table) {
             $table->renameColumn('has_subnavmenu', 'sequence');
        });
    }
}
