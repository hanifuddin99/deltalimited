<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubnavmenusUrlToSubnavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subnavmenus', function (Blueprint $table) {
            $table->string('subnavmenus_url',180)->nullable()->after('has_subnavmenus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subnavmenus', function (Blueprint $table) {
            $table->dropColumn('subnavmenus_url');
        });
    }
}
