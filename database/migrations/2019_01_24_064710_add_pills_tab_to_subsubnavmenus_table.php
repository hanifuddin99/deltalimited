<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPillsTabToSubsubnavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subsubnavmenus', function (Blueprint $table) {
            $table->string('pills_tab',150)->after('subsubnavmenu_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subsubnavmenus', function (Blueprint $table) {
            //
        });
    }
}
