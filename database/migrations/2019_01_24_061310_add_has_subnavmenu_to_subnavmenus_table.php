<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHasSubnavmenuToSubnavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subnavmenus', function (Blueprint $table) {
            $table->tinyInteger('has_subnavmenus')->after('subnavmenu_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subnavmenus', function (Blueprint $table) {
            //
        });
    }
}
