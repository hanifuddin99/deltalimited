<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNavmenuUrlToNavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('navmenus', function (Blueprint $table) {
            $table->string('navmenu_url')->after('has_subnavmenu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('navmenus', function (Blueprint $table) {
            $table->dropColumn('navmenu_url');
        });
    }
}
