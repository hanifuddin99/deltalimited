<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsubnavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subsubnavmenus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subnavmenu_id');
            $table->string('subsubnavmenu_name',150);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('subnavmenu_id')->references('id')->on('subnavmenus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subsubnavmenus');
    }
}
