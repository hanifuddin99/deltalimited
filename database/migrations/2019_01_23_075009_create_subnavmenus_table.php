<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubnavmenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subnavmenus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('navmenu_id');
            $table->string('subnavmenu_name',100);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('navmenu_id')->references('id')->on('navmenus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subnavmenus');
    }
}
