<?php

use Faker\Generator as Faker;

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->text,
        'image' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
