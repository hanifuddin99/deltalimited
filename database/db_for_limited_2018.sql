-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2019 at 02:21 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_for_limited_2018`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `album_name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'agm-2018', '2018-11-14-07-14-55.JPG', '2019-04-30 18:00:00', NULL),
(2, 'our finished project', '2018-11-14-07-18-08.JPG', '2019-04-30 18:00:00', NULL),
(3, 'ASIA Expo 2017', '2018-11-14-07-18-46.JPG', '2019-04-30 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `main_category_id` int(10) UNSIGNED DEFAULT NULL,
  `category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `main_category_id`, `category_name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'washing', NULL, '2018-10-13 18:00:00', '2018-10-13 18:00:00'),
(2, 2, 'chemical', NULL, '2018-10-13 18:00:00', '2018-10-13 18:00:00'),
(3, 1, 'medical_gas_pipe', NULL, '2018-10-13 18:00:00', '2018-10-13 18:00:00'),
(4, 1, 'hospital_bed', NULL, '2019-01-12 18:00:00', '2019-01-12 18:00:00'),
(5, 1, 'cssd_equipment', NULL, '2019-01-12 18:00:00', '2019-01-12 18:00:00'),
(6, 1, 'nurse_calling', NULL, '2019-01-12 18:00:00', '2019-01-12 18:00:00'),
(7, 1, 'coronary_stent', NULL, '2019-01-12 18:00:00', '2019-01-12 18:00:00'),
(8, 2, 'kitchen_equipment', NULL, '2019-01-12 18:00:00', '2019-01-12 18:00:00'),
(9, 2, 'dish_washing', NULL, '2019-01-12 18:00:00', '2019-01-12 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `image`, `url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'uploads/clients/aysha_logo.png', 'http://www.umchltd.com/', '2019-01-27 03:31:15', NULL, NULL),
(2, 'uploads/clients/a_specialized.jpg', 'http://labaidgroup.com/', '2019-01-27 03:32:53', NULL, NULL),
(3, 'uploads/clients/enam_medical.jpg', 'http://emcbd.com/', '2019-01-27 03:34:08', NULL, NULL),
(4, 'uploads/clients/metro_politon.png', 'http://mmclbd.net/', '2019-01-27 03:34:52', NULL, NULL),
(5, 'uploads/clients/bsh.jpg', 'http://www.bdspecializedhospital.com/', '2019-01-27 03:35:44', NULL, NULL),
(6, 'uploads/clients/ibnsinalogo.png', 'http://www.ibnsinatrust.com/', '2019-01-27 03:36:30', NULL, NULL),
(7, 'uploads/clients/logo-kpj.png', 'http://www.kpjdhaka.com/', '2019-01-27 03:37:29', NULL, NULL),
(8, 'uploads/clients/BRB-3-01.png', 'http://brbhospital.com/', '2019-01-27 03:38:15', NULL, NULL),
(9, 'uploads/clients/Hotel_Long_Beatch.jpg', 'https://longbeachsuitesbd.com/', '2019-01-27 22:29:55', NULL, NULL),
(10, 'uploads/clients/Mount-Adora-Hospital-Sylhet.jpg', 'http://www.mountadora.com/', '2019-01-27 22:32:16', NULL, NULL),
(11, 'uploads/clients/Shahid_Tazuddin_Medical_College_Hospital.jpeg', 'https://www.facebook.com/ShaheedTajuddinAhmadMadicalCollegeHospital/', '2019-01-27 22:36:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE `contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'About Us', '<h4 class=\'content-header\'>Delta Limited in Brief</h4>\r\n<hr />\r\n<p class=\'about-content\'>\r\nDelta Limited is the brain child of Delta Pharma Ltd. Year \r\nof establishment of Delta Limited is 2011. Both Delta Pharma Ltd &\r\n Delta Ltd is a Public Limited Company (Non Listed). Maximum shareholders\r\n of Delta Pharma Ltd & Delta Ltd are physicians and health related professionals &\r\n they have a strong urge for striving for the well-being of the human kind and serve\r\n the people of the country and offer them high quality healthcare services. \r\n Delta Limited has already taken the initiative of establishing five Delta\r\n Health Cares at different locations of Bangladesh with the involvement of local \r\n physicians, namely: Delta Health Care, Mirpur Limited, Dhaka (100 Bed Specialized Hospital)\r\n Delta Health Care, Mymensingh Limited, Mymensingh (100 Bed Specialized Hospital) Delta Health Care,\r\n Jatrabari Limited, Dhaka (50 Bed Specialized Hospital) Delta Health Care, Chittagong Limited, Chittagong \r\n(100 Bed Specialized Hospital) Delta Health Care, Rampura Limited, Dhaka (100 Bed Specialized Hospital).\r\n From 2014 all Five Hospitals are successfully running.  \r\n </p>\r\n <p class=\'about-content\'>\r\n	Delta Limited is a Service provider to Healthcare sector providing professional consultancy for\r\n	establishing Hospitals also providing supply, after sale\r\n	service, maintenance service for various Medical equipment.	\r\n </p>\r\n <p class=\'about-content\'>\r\n	Delta Limited is the leading supplier of Hospital Laundry \r\n	Equipment to this industry, also supply Laundry equipment, \r\n	Laundry chemical, House-keeping chemicals, Kitchen equipment’s\r\n	including Dish Washing equipment to various industries like Pharmaceuticals, \r\n	Hotels, Resorts, Food processing etc. 	\r\n </p>\r\n <h4 class=\'content-header\'>Basic Info.</h4>\r\n<hr />\r\n\r\n<dl>\r\n	<dt>Year of Establishment:</dt>\r\n	<dd>2011</dd>\r\n	<dt>Status:</dt>\r\n	<dd>Public Limited Company (Non Listed)</dd>\r\n	<dt>Business Line:</dt>\r\n	<dd>Health Care provider </dd>\r\n	<dt>Authorized Share Capital:</dt>\r\n	<dd>Taka 1,000 Million</dd>\r\n	<dt>Paid-up Share Capital:</dt>\r\n	<dd>Taka 65.85 Million (Up to the Book Closure Date of 23-25 	October 2018)</dd>\r\n	<dt>Number of Shareholders:</dt>\r\n	<dd>178 (Up to the Book Closure Date of 23-25 October	2018)</dd>\r\n	<dt>Number of Employees:</dt>\r\n	<dd>05 Persons Up to 30 June 2018</dd>\r\n</dl>\r\n\r\n<h4 class=\'content-header\'>Board of Directors</h4>\r\n<hr />\r\n<div class=\"table-responsive\">\r\n    <table class=\"table\">\r\n      <thead>\r\n        <tr>\r\n			<th>Director\'s Name</th>\r\n			<th>------</th>\r\n			<th>Designation</th>\r\n		</tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr class=\"table-info\">\r\n        <td>Prof. Abdul Bayes Bhiuyan</td>\r\n        <td>------</td>\r\n        <td>Chairman</td>\r\n      </tr>      \r\n      <tr class=\"table-primary\">\r\n        <td>Prof. Dr. Abu Zafar Md. Zahid Hossain</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-success\">\r\n        <td>Mrs. Rifat Hossain</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-danger\">\r\n        <td>Mrs. Nazneen Akhter</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-info\">\r\n        <td>Dr. Md. Abdul Mannan Miah</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-warning\">\r\n        <td>Prof. Dr. AKM  Fazlul Haque</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-active\">\r\n        <td>Prof. Dr. Md. Manzur Morshed</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-secondary\">\r\n        <td>Prof. Dr. Naima  Muazzam</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-danger\">\r\n        <td>Prof. Dr. Md. Zahangir Kabir</td>\r\n        <td>------</td>\r\n        <td>Director</td>\r\n      </tr>\r\n      <tr class=\"table-warning\">\r\n        <td>Dr. Md. Zakir Hossain</td>\r\n        <td>------</td>\r\n        <td>Managing Director</td>\r\n      </tr>\r\n      </tbody>\r\n    </table>\r\n  </div>\r\n  \r\n<h4 class=\'content-header\'>Executive Committee</h4>\r\n<hr />\r\n  <div class=\"table-responsive\">\r\n    <table class=\"table\">\r\n      <thead>\r\n        <tr>\r\n			<th>Member\'s Name</th>\r\n			<th>------</th>\r\n			<th>Designation</th>\r\n		</tr>\r\n      </thead>\r\n      <tbody>\r\n        <tr class=\"bg-primary\">\r\n        <td>Prof. Dr. Abu Zafar Md. Zahid Hossain</td>\r\n        <td>------</td>\r\n        <td>Chairman</td>\r\n      </tr>      \r\n      <tr class=\"bg-success\">\r\n        <td>Prof. Abdul Bayes Bhiuyan</td>\r\n        <td>------</td>\r\n        <td>Member</td>\r\n      </tr>\r\n      <tr class=\"bg-warning\">\r\n        <td>Dr. Md. Zakir Hossain</td>\r\n        <td>------</td>\r\n        <td>Member Secretary</td>\r\n      </tr>\r\n        </tbody>\r\n    </table>\r\n  </div>\r\n  <h4 class=\'content-header\'>Company Secretary (Honorary)</h4>\r\n<hr />\r\n<p class=\'about-content\'>Dr. A.K.M. Muslem Uddin</p>\r\n <h4 class=\'content-header\'>Auditors</h4>\r\n<hr /> \r\n<p class=\'about-content\'>M/s. Ahsan Manzur & Co.</p>\r\n<p class=\'about-content\'>Chartered Accountants</p>\r\n<h4 class=\'content-header\'>Legal Advisors</h4>\r\n<hr /> \r\n<p class=\'about-content\'>Dr. Naim Ahmed</p>\r\n<p class=\'about-content\'>S. Ahmed & Associates</p>\r\n<p class=\'about-content\'>166/1 Mirpur Road, Kolabagan, Dhaka-1205</p>\r\n\r\n<h4 class=\'content-header\'>Bankers</h4>\r\n<hr />\r\n<div class=\'table-responsive\'>\r\n<table class=\"table table-hover\">\r\n  <thead>\r\n    <tr>\r\n      <th scope=\"col\">Bankers Name</th>\r\n      <th scope=\"col\">------</th>\r\n      <th scope=\"col\">Address</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr>\r\n      <td>Islami Bank Bangladesh Ltd.</td>\r\n      <td>------</td>\r\n      <td>Mouchak Branch, Dhaka</td>\r\n    </tr>\r\n    <tr>\r\n      <td>AB Bank Limited</td>\r\n      <td>------</td>\r\n      <td>Kakrail Branch, Dhaka</td>\r\n    </tr>\r\n   </tbody>\r\n</table>\r\n</div>\r\n  ', '2018-11-13-07-00-38.png', '2018-10-13 18:00:00', '2018-10-13 18:00:00'),
(2, 'chairman\'s message', '<h4 class=\'text-center content-header\' style=\'color:white\'>CHAIRMAN\'S STATEMENT</h4>\r\n<p class=\'text-center\' style=\'color:white\'>Bismillahir Rahmanir Rahim</p>\r\n\r\n\r\n<p style=\'color:white\'>Dear Shareholders,</p>\r\n<p style=\'color:white\'>As-Salamu-Alaiqum.</p>\r\n<p style=\'color:white\'>\r\nI warmly welcome all of you to the Seventh Annual General Meeting of Delta Limited.\r\n With the blessing of Almighty we are in the service of man-kind for long time and \r\n we will be flourishing for many more years. I am delight to inform you that this is \r\n the first year, we made some operational income. I firmly believe that we have the \r\n right strategy and people in place to enable us to grow in the future. We have \r\n accelerated and expanded our business operations to keep growing our company and\r\n continues growth momentum. Upon review of the Directors’ Report and the Managing \r\n Director’s Statement, you will note that we have performed well in our operational \r\n and financial continuum with sales, gross and net profit from the comparable previous period.\r\n </p>\r\n<p style=\'color:white\'>\r\nUnder the direct contribution and supervision of Board and management, 5 large scale \r\nhospitals have been established in Dhaka and out of Dhaka. They are growing steadily. \r\nBesides the supervision of hospitals, our activities are - providing medical equipment \r\nand services. Quality is our main focus then profit. Some renowned hospital in the\r\n country have been taking medical equipment and services from us. The business\r\n activities are expanding in a professional way.\r\n </p>\r\n<p style=\'color:white\'> \r\nWe performed well in the past year and I remain confident that the Board of \r\nDirectors are taking the right actions to grow and deliver results in the coming \r\nyears as well. Our adopted strategies squarely fit to our goal to build Delta Ltd.\r\n as one of the prestigious and iconic organization in the country.\r\n </p>\r\n<p style=\'color:white\'>\r\nI must say we have got a dedicated, capable and very sincere pool of employees.\r\n The achievement we have made so far was only possible because of such a talented team. \r\n I thankfully acknowledge their contribution to the company. We are grateful to our \r\n shareholders for extending, at all times, their invaluable support and cooperation \r\n to bring the company to the level it has reached today.\r\n </p>\r\n<p style=\'color:white\'> \r\nThanking you all with best wishes of health and heart.\r\n</p>\r\n<p style=\'color:white\'>\r\nThanking you.\r\n</p>\r\n<p style=\'color:white\'>\r\nSincerely yours\r\n</p>\r\n<p style=\'color:white\'>\r\n <img src=\"http://localhost/deltalimited/public/uploads/contents/chairman_signature.png\" alt=\'\' />\r\n</p>\r\n<p class=\"font-weight-bold\" style=\'color:white\'>\r\nProf. Dr. Abdul Bayes Bhuiyan\r\n</p>\r\n', 'chairman_image.png', '2019-04-03 18:00:00', NULL),
(3, 'managing_director\'s_message', '<h4 class=\'text-center content-header\' style=\'color:white\'>ADDRESS OF WELCOME BY THE MANAGING DIRECTOR</h4>\r\n<p class=\'text-center\' style=\'color:white\'>Bismillahir Rahmanir Rahim</p>\r\n\r\n\r\n\r\n\r\n\r\n<p style=\'color:white\'>Honorable Chairman, Directors, Shareholders and the colleagues,</p>\r\n<p style=\'color:white\'>As-Salamu-Alaiqum.</p>\r\n<p style=\'color:white\'>\r\nIt\'s my pleasure to welcome you all to the 7th Annual General Meeting. \r\nWe are placing the Annual Report for the period ended 30th June, 2018 \r\ncontaining the Director\'s report, Statement of Financial Position, Statement \r\nof Comprehensive Income, Statement of Cash Flows and Auditors Report for your consideration and approval.\r\n</p>\r\n<p style=\'color:white\'>\r\nI hope, you will be happy to see the results of this year\'s business performance. \r\nThe most satisfying performance of this year is that, we have earned some profit\r\n before Tax and we are close to operational profit level. We are going to have\r\n the foundation of a business which will have the potential to grow into a large \r\n segment of the medical sector. Thanks for your patience. In the years to come,\r\n you will find yourselves adequately rewarded for your foresight. Delta Limited has\r\n started to cover almost the entire range of products and services of health sector. \r\n We are helping them to earn a better return from their service by supplying them\r\n with quality products and encouraging them to adopt modern practices. We acknowledge\r\n with deep gratitude the level of trust our customers have in our products and services.\r\n Customer Focus is one of our values and we try to maintain that trust through rigid\r\n Quality Control and prompt after sales service. \r\n </p>\r\n <p style=\'color:white\'>\r\nDear Shareholders, our country is developing at a fast pace. When we reach the earning\r\n level of a middle income country, the huge domestic market will change the dynamics of\r\n many of our businesses and create prospects for existing as well as many yet-to-evolve \r\n businesses. The opportunities will be there for those who will have the foresight and capacity.\r\n Your management has the vision and the willingness to convert the opportunities to economic values.\r\n Your investments are in good hands and I am certain, will grow at a rapid pace.\r\n The strategic vision with which this company is being led by its Managing Director \r\n and his Executive team is recognized and appreciated. The Board has acknowledged\r\n their performance from time to time. Excellence at the operational level has also\r\n equally contributed to the success. On behalf of the Board and on my own behalf \r\n I thank employees at all levels for their sincerity, efficiency and hard work. \r\n </p>\r\n <p style=\'color:white\'>\r\nWe have reached 07 years of our journey. The opportunities, which are coming to us, \r\ngives me the strong belief that we will be able to serve our shareholders happily in near future.\r\n I want our investors to realize that through Delta Limited, they are participating in \r\n creating wealth for this nation and bringing in prosperity for our people.\r\n </p>\r\n <p style=\'color:white\'>\r\nMy honest and sincere gratitude to all of you,\r\n</p>\r\n<p style=\'color:white\'>\r\n<img src=\"http://localhost/deltalimited/public/uploads/contents/md_signature.png\" alt=\'\' />\r\n</p>\r\n<p class=\"font-weight-bold\" style=\'color:white\'>\r\nDr. Md. Zakir Hossain\r\n</p>\r\n', 'chairman_image.png', '2019-04-03 18:00:00', NULL),
(4, 'vission_mission', '<h4 class=\'content-header\'>Our Vision</h4>\r\n<p>\r\nOur vision is to reach the level of excellence in the field of Health\r\n service delivery, Human resource development and scientific researches\r\n through sustained effort and ensuring quality and to achieve a global\r\n standard through the indoctrination of culture of excellence.\r\n </p>\r\n<h4 class=\'content-header\'>Our Mission</h4>\r\n<p>\r\nOur mission is to benefit the country people and improve their quality\r\n of life through our excellent services. We would like to ensure customer \r\n satisfaction through providing quality services at an affordable cost.\r\n Our mission is to establish academic institutions, training & research\r\n organization for developing qualified & skilled human resources \r\n in all possible avenues. We want to inculcate new & innovative ideas.\r\n We are committed to achieve our goal through skilled, creative and\r\n motivated employees of highest professional standard.\r\n </p>\r\n', '2018-11-13-07-00-38.png', '2019-04-03 18:00:00', NULL),
(5, 'board_of_directors', 'this is test', 'board_directors.png', '2019-04-24 00:58:23', '2019-04-24 00:58:23');

-- --------------------------------------------------------

--
-- Table structure for table `directors`
--

CREATE TABLE `directors` (
  `id` int(10) UNSIGNED NOT NULL,
  `director_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `directors`
--

INSERT INTO `directors` (`id`, `director_name`, `designation`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Prof. Abdul Bayes Bhiuyan', 'chairman', 'This is test', 'uploads/directors/Abdul Bayes Sir.jpg', '2019-04-21 06:22:56', '2019-04-21 06:22:56'),
(3, 'Dr. Md. Zakir Hossain', 'managing director', 'this is test', 'uploads/directors/md_sir.jpg', '2019-04-24 04:23:55', '2019-04-24 04:23:55'),
(4, 'Prof. Dr. Abu Zafar Md. Zahid Hossain', 'director', 'this is test', 'uploads/directors/Zahid Sir.jpg', '2019-04-24 06:06:33', '2019-04-24 06:06:33'),
(5, 'Mrs. Rifat Hossain', 'director', 'this is test', 'uploads/directors/Rifat Hossain.jpg', '2019-04-24 06:06:48', '2019-04-24 06:06:48'),
(6, 'Prof. Dr. Razibul Alom', 'director', 'this is test', 'uploads/directors/Razib Sir.jpg', '2019-04-24 22:55:52', '2019-04-24 22:55:52'),
(7, 'Dr. Md. Abdul Mannan Miah', 'director', 'this is test', 'uploads/directors/Abdul Mannan.jpg', '2019-04-25 02:00:25', '2019-04-25 02:00:25'),
(8, 'Prof. Dr. AKM Fazlul Haque', 'director', 'this is test', 'uploads/directors/Fazlul Haque.JPG', '2019-04-25 02:01:03', '2019-04-25 02:01:03'),
(9, 'Prof. Dr. Md. Manzur Morshed', 'director', 'this is test', 'uploads/directors/Manzur Morshed.JPG', '2019-04-25 02:01:32', '2019-04-25 02:01:32'),
(10, 'Prof. Dr. Naima Muazzam', 'director', 'this is test', 'uploads/directors/Naima Muazzam.jpg', '2019-04-25 02:02:03', '2019-04-25 02:02:03'),
(11, 'Prof. Dr. Md. Zahangir Kabir', 'director', 'this is test', 'uploads/directors/Jahangir Kabir.jpg', '2019-04-25 02:02:33', '2019-04-25 02:02:33');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `album_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `album_id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 1, 'Delta Limited AGM', '2018-11-14-07-14-55.JPG', '2018-10-22 03:37:33', '2018-10-22 03:37:33'),
(2, 1, 'Delta Limited Board Members', '2018-11-14-07-16-30.JPG', '2018-10-22 03:37:33', '2018-10-22 03:37:33'),
(3, 1, 'Delta Limited MD Speech', '2018-11-14-07-16-39.JPG', '2018-10-22 03:37:33', '2018-10-22 03:37:33'),
(4, 1, 'Delta Limited Director speech', '2018-11-14-07-16-49.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(5, 1, 'Delta Limited MD Speech', '2018-11-14-07-17-01.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(6, 1, 'Delta Limited Shareholders', '2018-11-14-07-17-16.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(7, 1, 'Delta Limited AGM', '2018-11-14-07-17-39.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(8, 2, 'Universal Medical College Washing Plant', '2018-11-14-07-17-52.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(9, 2, 'Metropolitan Medical Center Washing Plant', '2018-11-14-07-18-08.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(10, 2, 'Labaid Hospital Washing Unit', '2018-11-14-07-18-21.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(11, 2, 'Bangladesh Specialized Hospital Sterilized Unit', '2018-11-14-07-18-35.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(12, 3, 'ASIA Expo 2017', '2018-11-14-07-18-45.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(13, 3, 'ASIA Expo 2017', '2018-11-14-07-18-46.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(14, 3, 'ASIA Expo 2017', '2018-11-14-07-18-47.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34'),
(15, 3, 'ASIA Expo 2017', '2018-11-14-07-18-48.JPG', '2018-10-22 03:37:34', '2018-10-22 03:37:34');

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `category_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'medical', '2019-03-03 18:00:00', '2019-03-03 18:00:00', NULL),
(2, 'industrial', '2019-03-03 18:00:00', '2019-03-03 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2018_10_17_054932_create_categories_table', 1),
(6, '2018_10_21_102126_create_news_table', 1),
(7, '2018_10_22_075933_create_abouts_table', 1),
(8, '2018_10_14_061419_create_products_table', 2),
(16, '2018_10_22_091244_create_images_table', 3),
(17, '2018_11_01_082033_add_url_to_products_table', 4),
(18, '2019_01_23_055856_create_navmenus_table', 5),
(19, '2019_01_23_075009_create_subnavmenus_table', 6),
(20, '2019_01_23_084647_create_subsubnavmenus_table', 7),
(21, '2019_01_24_050146_rename_sequence_column_to_navmenus_table', 8),
(22, '2019_01_24_061310_add_has_subnavmenu_to_subnavmenus_table', 9),
(24, '2019_01_24_063449_add_pills_tab_to_subsubnavmenus_table', 10),
(25, '2019_01_24_064710_add_pills_tab_to_subsubnavmenus_table', 11),
(26, '2019_01_27_054322_add_navmenu_url_to_navmenus_table', 12),
(27, '2019_01_27_081128_create_clients_table', 13),
(28, '2019_01_29_114226_add_subnavmenus_url_to_subnavmenus_table', 14),
(29, '2019_02_03_094658_create_videos_table', 15),
(30, '2019_02_07_065234_add_sub_pro_name_to_products_table', 16),
(45, '2019_03_04_065602_create_main_categories_table', 17),
(52, '2019_03_05_053654_add_foreignkey_to_categoies_table', 18),
(53, '2019_03_12_052033_rename_table_abouts_to_contents', 19),
(54, '2019_04_04_085828_add_foreign_key_to_subnavmenus_table', 20),
(55, '2019_04_11_044008_add_foreignkey_to_subsubnavmenus_table', 21),
(56, '2019_04_21_062253_create_albums_table', 22),
(57, '2019_04_21_121100_create_directors_table', 23),
(58, '2019_04_21_122402_add_designation_to_directors_table', 24),
(59, '2019_04_23_054621_rename_directors_table', 25),
(60, '2019_04_24_063019_reorganize_order_of_column_designation', 26),
(62, '2019_05_01_041442_add_foreignkey_to_images_table', 27);

-- --------------------------------------------------------

--
-- Table structure for table `navmenus`
--

CREATE TABLE `navmenus` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_subnavmenu` tinyint(1) NOT NULL,
  `navmenu_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `navmenus`
--

INSERT INTO `navmenus` (`id`, `menu_name`, `has_subnavmenu`, `navmenu_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'home', 0, '/', '2019-01-23 01:16:56', '2019-01-23 01:16:56', NULL),
(2, 'about_us', 1, '', '2019-01-23 01:17:47', '2019-01-23 01:17:47', NULL),
(3, 'products', 1, '', '2019-01-23 01:18:11', '2019-01-23 01:18:11', NULL),
(4, 'gallery', 1, '', '2019-01-23 01:18:48', '2019-01-23 01:18:48', NULL),
(5, 'contact', 0, 'contact', '2019-01-23 01:19:31', '2019-01-23 01:19:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'this is test', 'this is test', 'uploads/news/DSC_0507.JPG', '2019-01-31 00:53:11', NULL),
(2, 'this is test', 'this is test', 'uploads/news/DSC_0584.JPG', '2019-01-31 00:53:25', NULL),
(3, 'this is test', 'this is test', 'uploads/news/DSC_0552.JPG', '2019-01-31 00:53:35', NULL),
(4, 'this is test', 'this is test', 'uploads/news/DSC_0599.JPG', '2019-01-31 00:53:43', NULL),
(5, 'this is test', 'this is test', 'uploads/news/DSC_0615.JPG', '2019-01-31 00:53:51', NULL),
(6, 'this is test', 'this is test', 'uploads/news/DSC_0628.JPG', '2019-01-31 00:53:58', NULL),
(7, 'this is test', 'this is test', 'uploads/news/DSC_0636.JPG', '2019-01-31 00:54:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `pro_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_pro_name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `pro_name`, `sub_pro_name`, `brand`, `origin`, `image`, `url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Washer Extractor', NULL, 'IMESA', 'ITALY', 'uploads/products/IMESA_Washer.png', 'http://www.imesa.it/it/prodotti/lavatrici/', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(2, 1, 'Tumble Dryer', NULL, 'IMESA', 'ITALY', 'uploads/products/IMESA_Laundry_Dryer.png', 'http://www.imesa.it/en/products/tumble-dryers/', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(3, 1, 'Flatwork Ironer', NULL, 'IMESA', 'ITALY', 'uploads/products/IMESA_Drying_Ironer.jpg', 'http://www.imesa.it/it/prodotti/mangani/', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(4, 2, 'ALKALINE BOOSTER', NULL, 'Atlantic Care', 'Canada/India', 'uploads/products/ALKALINE BOOSTER.jpg', 'http://www.atlanticchemicals.com/', '2018-10-13 18:00:01', '2018-10-13 18:00:00', NULL),
(5, 5, 'Steam Sterilizer', NULL, 'ERNA', 'TURKEY', 'uploads/products/Steam_Sterilizer.jpg', '', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(6, 5, 'ETO Sterilizer', NULL, 'sterile safequip', 'INDIA', 'uploads/products/ETO_Sterilizer.jpg', '', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(7, 3, 'Central Medical Gas Pipe line materials', NULL, 'Deltap S.r.l', 'italy', 'uploads/products/Gas_pipe_Fittings.jpg', 'https://www.deltap.it/', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(8, 3, 'Gas Manifold', NULL, 'deltap s.r.l', 'italy', 'uploads/products/Medical_Gas_Pipe.png', 'https://www.deltap.it/', '2018-10-13 18:00:00', '2018-10-13 18:00:00', NULL),
(9, 5, 'Surgical Instrument Washer', NULL, 'tbt', 'turkey', 'uploads/products/Instrument_Washer-01.jpg', '', '2019-01-12 18:00:00', NULL, NULL),
(10, 5, 'Surgical Instrument Washer', NULL, 'tbt', 'turkey', 'uploads/products/Instrument_Washer-020.jpg', '', '2019-01-12 18:00:00', NULL, NULL),
(11, 5, 'plasma STERILIZER', NULL, 'pms', 'turkey', 'uploads/products/ETHYLENE OXIDE STERILIZER.jpg', '', '2019-01-12 18:00:00', NULL, NULL),
(12, 8, 'Burner', NULL, 'offcar', 'italy', 'uploads/products/GAS_RANGES.jpg', '', '2019-01-12 18:00:00', NULL, NULL),
(13, 8, 'Fryers', NULL, 'offcar', 'italy', 'uploads/products/FRYERS.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(14, 8, 'PASTA COOKER', NULL, 'offcar', 'italy', 'uploads/products/PASTA_COOKER.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(15, 8, 'WATER-HEATED RADIATING GRID', NULL, 'offcar', 'italy', 'uploads/products/WATER-HEATED_RADIATING_GRID.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(16, 1, 'Ironing Table', NULL, 'imesa', 'italy', 'uploads/products/Ironing_Table_11.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(17, 1, 'Ironing table', NULL, 'imesa', 'italy', 'uploads/products/Ironing-table-professional-full-function-fabric-steam.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(18, 6, 'Bed Call unit', NULL, 'deling electroincs', 'china', 'uploads/products/Bed_Call_unit.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(19, 6, 'Nurse Calling System', NULL, 'deling electroincs', 'china', 'uploads/products/Nurse_Calling_System.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(20, 6, 'Nurse Station Master Set', NULL, 'deling electroincs', 'china', 'uploads/products/Phone_HeadSet.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(21, 1, 'Utility Press', NULL, 'imesa', 'ITALY', 'uploads/products/Utility_Press-11.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(22, 9, 'Rack Conveyor Washer', NULL, 'commenda', 'italy', 'uploads/products/Rack Conveyor Washer-2.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(23, 9, 'Dish Washing', NULL, 'commenda', 'italy', 'uploads/products/Pic_2.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(24, 9, 'Dish Washing', NULL, 'commenda', 'italy', 'uploads/products/Pic_3.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(25, 9, 'Dish Washing', NULL, 'commenda', 'italy', 'uploads/products/Pic_4.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(26, 9, 'Flight Conveyor Dish washer', NULL, 'commenda', 'italy', 'uploads/products/Flight_Conveyor_Dish_washer-08.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(27, 9, 'Front loading Fixed rack Dish washer', NULL, 'commenda', 'italy', 'uploads/products/Front_loading_Fixed_rack_Dish_washer-3.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(28, 9, 'Front loading Fixed rack Dish washer', NULL, 'commenda', 'italy', 'uploads/products/Front_loading_Fixed_rack_Dish_washer-4.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(29, 9, 'Front loading Fixed rack Dish washer', NULL, 'commenda', 'italy', 'uploads/products/Front_loading_Fixed_rack_Dish_washer-5.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(31, 3, 'Medical Gas Plant', NULL, 'deltap s.r.l', 'italy', 'uploads/products/Medical Gas Plant-4.jpg', 'https://www.deltap.it/', '2019-01-13 18:00:00', NULL, NULL),
(32, 3, 'MEDICAL GAS ACCESSORIES', NULL, 'deltap s.r.l', 'italy', 'uploads/products/Outlets-2.jpg', 'https://www.deltap.it/', '2019-01-13 18:00:00', NULL, NULL),
(33, 4, 'Electric Bed', NULL, '', 'Turkey/China', 'uploads/products/Electric Bed-1.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(34, 4, 'ICU Bed', NULL, '', 'Turkey/China', 'uploads/products/ICU Bed-1.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(35, 4, 'Manual Bed', NULL, '', 'Turkey/China', 'uploads/products/Manual Bed copy.jpg', '', '2019-01-13 18:00:00', NULL, NULL),
(37, 2, 'Liquid softener with perfume and neutralizer (BIO SOFT TB)', NULL, 'Atlantic Care', 'Canada/India', 'uploads/products/BIO SOFT TB-I.jpg', 'http://www.atlanticchemicals.com/', '2018-10-13 18:00:04', NULL, NULL),
(38, 2, 'Chlorine Bleach (BIO SUPERCHLOR)', NULL, 'Atlantic Care', 'Canada/India', 'uploads/products/BIO SUPERCHLOR.jpg', 'http://www.atlanticchemicals.com/', '2018-10-13 18:00:03', NULL, NULL),
(39, 2, 'Oxygen Bleach (OXY BRIGHT)', NULL, 'Atlantic Care', 'Canada/India', 'uploads/products/OXY BRIGHT.jpg', 'http://www.atlanticchemicals.com/', '2018-10-13 18:00:02', NULL, NULL),
(40, 2, 'Emulsifier (BIO MOUL EM)', NULL, 'ATLANTIC CARE', 'CANADA/INDIA', 'uploads/products/BIO MOUL EM.jpg', 'http://www.atlanticchemicals.com/', '2018-10-13 18:00:00', NULL, NULL),
(41, 2, 'SC-01(A) (Toilet/Washroom cleaner)', NULL, 'ATLANTIC CARE', 'CANADA/INDIA', 'uploads/products/SC-01A.jpg', 'http://www.atlanticchemicals.com/', '2019-01-21 18:00:00', NULL, NULL),
(42, 2, 'SC-2 (Toilet Cleaner)', NULL, 'ATLANTIC CARE', 'CANADA/INDIA', 'uploads/products/SC_2_(Toilet_Cleaner_Sanitizer).jpg', 'http://www.atlanticchemicals.com/', '2019-01-21 18:00:00', NULL, NULL),
(43, 2, 'SC-3 (Glass and Mirror Cleaner)', NULL, 'ATLANTIC CARE', 'CANADA/INDIA', 'uploads/products/SC-03.jpg', 'http://www.atlanticchemicals.com/', '2019-01-21 18:00:00', NULL, NULL),
(44, 2, 'SC-4 (Air Freshner)', NULL, 'ATLANTIC CARE', 'CANADA/INDIA', 'uploads/products/SC-04.jpg', 'http://www.atlanticchemicals.com/', '2019-01-21 18:00:00', NULL, NULL),
(45, 2, 'SC-7 (Bacteridical heavy duty Degreaser)', NULL, 'ATLANTIC CARE', 'CANADA/INDIA', 'uploads/products/SC-07.jpg', 'http://www.atlanticchemicals.com/ 	', '2019-01-21 18:00:00', NULL, NULL),
(46, 7, 'drug eluting coronary stent', 'Affinity CC', 'umbra medical', 'usa', 'uploads/products/Affinity_CC.jpg', '', '2019-01-26 18:00:00', NULL, NULL),
(47, 7, 'drug eluting coronary stent', 'Affinity MS', 'umbra medical', 'usa', 'uploads/products/Affinity_MS.jpg', '', '2019-01-26 18:00:00', NULL, NULL),
(48, 6, 'Corridor Display', NULL, 'DELING ELECTROINCS', 'CHINA', 'uploads/products/Corridoor_Disply_(Nurse_Call_System).jpg', '', '2019-01-26 18:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subnavmenus`
--

CREATE TABLE `subnavmenus` (
  `id` int(10) UNSIGNED NOT NULL,
  `navmenu_id` int(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `subnavmenu_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `has_subnavmenus` tinyint(4) NOT NULL,
  `subnavmenus_url` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subnavmenus`
--

INSERT INTO `subnavmenus` (`id`, `navmenu_id`, `content_id`, `subnavmenu_name`, `has_subnavmenus`, `subnavmenus_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, NULL, 'medical', 1, NULL, '2019-01-23 02:42:39', '2019-01-23 02:42:39', NULL),
(2, 3, NULL, 'industrial', 1, NULL, '2019-01-23 02:43:05', '2019-01-23 02:43:05', NULL),
(3, 4, NULL, 'image', 0, 'photos', '2019-01-29 03:08:07', NULL, NULL),
(4, 4, NULL, 'video', 0, 'videos', '2019-01-29 03:09:11', NULL, NULL),
(5, 2, 2, 'message_from_the_chairman', 0, 'chairman\'s message', '2019-03-11 06:16:05', NULL, NULL),
(6, 2, 3, 'message_from_the_managing_director', 0, 'managing_director\'s_message', '2019-03-11 06:31:42', NULL, NULL),
(7, 2, NULL, 'board_of_directors', 0, 'board_of_directors', '2019-03-11 06:33:02', NULL, NULL),
(8, 2, 4, 'mission-Vission', 0, 'vission_mission', '2019-03-11 06:34:00', NULL, NULL),
(9, 2, 1, 'company_profile', 0, 'About Us', '2019-03-11 06:16:04', NULL, NULL),
(11, 3, NULL, 'all_products', 1, NULL, '2019-04-10 18:00:00', '2019-04-10 18:00:00', '2019-04-10 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subsubnavmenus`
--

CREATE TABLE `subsubnavmenus` (
  `id` int(10) UNSIGNED NOT NULL,
  `subnavmenu_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `subsubnavmenu_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pills_tab` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subsubnavmenus`
--

INSERT INTO `subsubnavmenus` (`id`, `subnavmenu_id`, `category_id`, `subsubnavmenu_name`, `pills_tab`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 4, 'hospital_bed', 'products#pills-hospital_bed', '2019-01-23 03:05:42', '2019-01-23 03:05:42', NULL),
(2, 1, 5, 'cssd', 'products#pills-cssd_equipment', '2019-01-23 03:06:13', '2019-01-23 03:06:13', NULL),
(3, 1, 3, 'gas_pipe_line', 'products#pills-medical_gas_pipe', '2019-01-23 03:06:49', '2019-01-23 03:06:49', NULL),
(4, 1, 7, 'coronary_stent', 'products#pills-coronary_stent', '2019-01-23 03:07:34', '2019-01-23 03:07:34', NULL),
(5, 1, 6, 'nurse_calling_system', 'products#pills-nurse_calling', '2019-01-23 03:08:24', '2019-01-23 03:08:24', NULL),
(6, 2, 1, 'laundry_washing_plant', 'products#pills-washing', '2019-01-23 03:09:23', '2019-01-23 03:09:23', NULL),
(7, 2, 2, 'laundry/house_keeping_chemical', 'products#pills-chemical', '2019-01-23 03:10:10', '2019-01-23 03:10:10', NULL),
(8, 2, 8, 'kitchen_equipment', 'products#pills-kitchen_equipment', '2019-01-23 03:18:02', '2019-01-23 03:18:02', NULL),
(9, 2, 9, 'dish_washing', 'products#pills-dish_washing', '2019-01-23 03:18:21', '2019-01-23 03:18:21', NULL),
(10, 11, NULL, 'medical', 'null', '2019-04-28 18:00:00', '2019-04-28 18:00:00', NULL),
(11, 11, NULL, 'industrial', 'Null', '2019-04-28 18:00:00', '2019-04-28 18:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Md. Hanif Uddin', 'hanif@gmail.com', NULL, '$2y$10$6nEEQIM5tL91Rm3UJc19sOjnHxBJHcRl7LJN/Qapa5KsIhKZC8WAe', 'lKywhWDBWymO0Q1umLhS6zLY3VxP7sMnF3YzcTawGSq0GhqpOj4lrhXGyWsa', '2018-10-16 05:10:27', '2018-10-16 05:10:27');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `video_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `video_url`, `video_text`, `created_at`, `updated_at`) VALUES
(1, 'https://www.youtube.com/embed/4yt3XKCfaoE?rel=0', 'How to wash , dry and iron cloth in imesa washing machine.', '2019-02-03 23:24:37', NULL),
(2, 'https://www.youtube.com/embed/ccbssFj8Xis?rel=0', 'How Comenda Conveyor Dishwasher works.', '2019-02-06 05:17:41', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_main_category_id_foreign` (`main_category_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `directors`
--
ALTER TABLE `directors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `images_album_id_foreign` (`album_id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `navmenus`
--
ALTER TABLE `navmenus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `subnavmenus`
--
ALTER TABLE `subnavmenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subnavmenus_navmenu_id_foreign` (`navmenu_id`),
  ADD KEY `subnavmenus_content_id_foreign` (`content_id`);

--
-- Indexes for table `subsubnavmenus`
--
ALTER TABLE `subsubnavmenus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subsubnavmenus_subnavmenu_id_foreign` (`subnavmenu_id`),
  ADD KEY `subsubnavmenus_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `directors`
--
ALTER TABLE `directors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `navmenus`
--
ALTER TABLE `navmenus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `subnavmenus`
--
ALTER TABLE `subnavmenus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `subsubnavmenus`
--
ALTER TABLE `subsubnavmenus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_main_category_id_foreign` FOREIGN KEY (`main_category_id`) REFERENCES `main_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_album_id_foreign` FOREIGN KEY (`album_id`) REFERENCES `albums` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `subnavmenus`
--
ALTER TABLE `subnavmenus`
  ADD CONSTRAINT `subnavmenus_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subnavmenus_navmenu_id_foreign` FOREIGN KEY (`navmenu_id`) REFERENCES `navmenus` (`id`);

--
-- Constraints for table `subsubnavmenus`
--
ALTER TABLE `subsubnavmenus`
  ADD CONSTRAINT `subsubnavmenus_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subsubnavmenus_subnavmenu_id_foreign` FOREIGN KEY (`subnavmenu_id`) REFERENCES `subnavmenus` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
